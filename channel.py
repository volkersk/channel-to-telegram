import codecs
import requests
from time import sleep

from constants import LND_API, LND_DIR, NODE_PUB_KEYS, REMOVE_ALIAS_PREFIX

LND_API = LND_API.rstrip('/')
LND_DIR = LND_DIR.rstrip('/')

CERT_PATH = LND_DIR + '/tls.cert'
MACAROON = codecs.encode(open(LND_DIR + '/data/chain/bitcoin/mainnet/admin.macaroon', 'rb').read(), 'hex')
HEADERS = {'Grpc-Metadata-macaroon': MACAROON}


class Channel(object):
    def __init__(
            self,
            pub_key: str,
            alias: str,
            capacity: int,
            remote_balance: int,
            local_balance: int
    ):
        self.pub_key = pub_key
        self.alias = alias.replace(REMOVE_ALIAS_PREFIX, '', 1)
        self.capacity = capacity
        self.remote_balance = remote_balance
        self.local_balance = local_balance

    def __eq__(self, other):
        if isinstance(other, Channel):
            return self.pub_key == other.pub_key
        return False


def parse_node_alias(unparsed_node) -> str:
    return unparsed_node['node']['alias'].replace(REMOVE_ALIAS_PREFIX, '', 1)


def get_api_node_alias(pub_key: str) -> str:
    api_url = LND_API + '/v1/graph/node/' + pub_key
    response = requests.get(api_url, headers=HEADERS, verify=CERT_PATH, params={'include_channels': 'false'})
    if response.status_code != 200:
        return pub_key

    json_response = response.json()
    alias = pub_key
    try:
        alias = parse_node_alias(json_response)
    except Exception as e:
        print(repr(e), 'Unable to parse node alias, skipping: {0}'.format(json_response))
    return alias


def parse_channel(unparsed_channel) -> Channel:
    return Channel(
        pub_key=unparsed_channel['remote_pubkey'],
        alias='',
        capacity=int(unparsed_channel['capacity']),
        remote_balance=int(unparsed_channel['remote_balance']),
        local_balance=int(unparsed_channel['local_balance'])
    )


def parse_channels_response(response) -> [Channel]:
    unparsed_channels = response['channels']
    channels = []
    for unparsed_channel in unparsed_channels:
        try:
            channel = parse_channel(unparsed_channel)
            channels.append(channel)
        except Exception as e:
            print(repr(e), 'Unable to parse channel, skipping: {0}'.format(unparsed_channel))
    return channels


def get_api_channels(pub_keys: [str]) -> ([Channel], [Channel]):
    api_url = LND_API + '/v1/channels'
    response = requests.get(api_url, headers=HEADERS, verify=CERT_PATH)
    if response.status_code != 200:
        return []

    json_response = response.json()
    parsed_channels = parse_channels_response(json_response)
    channels = [channel for channel in parsed_channels if channel.pub_key in pub_keys]
    return channels, parsed_channels


def get_channels() -> ([Channel], [Channel]):
    channels, all_channels = get_api_channels(NODE_PUB_KEYS)
    for channel in channels:
        sleep(1)
        channel.alias = get_api_node_alias(channel.pub_key)
    return channels, all_channels
