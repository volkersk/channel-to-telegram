import requests

from constants import TELEGRAM_BOT_KEY, TELEGRAM_CHAT_ID, TELEGRAM_ALIAS
from channel import Channel
from stats import Stats


def get_channel_balance_state_char(channel: Channel) -> str:
    balance_total = channel.local_balance + channel.remote_balance
    balance_ratio = float(channel.local_balance / balance_total)
    if balance_ratio >= 0.95:
        return '🔴'  # red
    if balance_ratio <= 0.05:
        return '🔴'  # red
    if balance_ratio >= 0.875:
        return '🟠'  # orange
    if balance_ratio <= 0.125:
        return '🟠'  # orange
    if balance_ratio >= 0.75:
        return '🟡'  # yellow
    if balance_ratio <= 0.25:
        return '🟡'  # yellow
    return '🟢'  # green


def send_channels(channels: [Channel], stats: Stats) -> int:
    message = '<b>{0}</b>\n'.format(TELEGRAM_ALIAS)
    for channel in channels:
        message += '\n{0} to <code>{1}</code>: {2:,} sat'.format(
            get_channel_balance_state_char(channel),
            channel.alias,
            channel.local_balance
        )

    message += '\n\nChannels: {0}\nCapacity: {1:.1f}M sat\nCap ratio: {2:.2f}x'.format(
        stats.channels,
        stats.capacity / 1_000_000,
        stats.capacity_outbound / stats.capacity_inbound
    )
    if stats.fees_earned_day is not None:
        message += '\nEarned day: {0:,} sat'.format(stats.fees_earned_day)
    if stats.fees_earned_week is not None:
        message += '\nEarned week: {0:,} sat'.format(stats.fees_earned_week)
    if stats.fees_earned_month is not None:
        message += '\nEarned month: {0:,} sat'.format(stats.fees_earned_month)

    return send_message(message)


def send_message(message: str) -> int:
    uri = 'https://api.telegram.org/bot{0}/sendMessage'.format(TELEGRAM_BOT_KEY)
    response = requests.post(uri, json={
        'chat_id': TELEGRAM_CHAT_ID,
        'text': message,
        'parse_mode': "HTML"
    })
    return response.status_code
