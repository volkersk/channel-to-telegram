from channel import get_channels
from stats import get_stats
from telegram import send_channels

if __name__ == '__main__':
    channels = []
    all_channels = []
    try:
        print('Getting channels...')
        channels, all_channels = get_channels()
        print('Finished getting channels ({0})'.format(len(channels)))
    except Exception as e:
        print(repr(e), 'Unable to get channels')
        quit()

    stats = None
    try:
        print('Getting stats...')
        stats = get_stats(all_channels)
        print('Finished getting stats')
    except Exception as e:
        print(repr(e), 'Unable to get stats')
        quit()

    try:
        print('Sending telegram message...')
        status = send_channels(channels, stats)
        print('Finished sending telegram message [{0}]'.format(status))
    except Exception as e:
        print(repr(e), "Unable to send telegram message")
        quit()
