import codecs
from datetime import datetime
from time import sleep

import requests

from channel import Channel

from constants import LND_API, LND_DIR

LND_API = LND_API.rstrip('/')
LND_DIR = LND_DIR.rstrip('/')

CERT_PATH = LND_DIR + '/tls.cert'
MACAROON = codecs.encode(open(LND_DIR + '/data/chain/bitcoin/mainnet/admin.macaroon', 'rb').read(), 'hex')
HEADERS = {'Grpc-Metadata-macaroon': MACAROON}


class Stats(object):
    def __init__(
            self,
            channels: int,
            capacity: int,
            capacity_outbound: int,
            capacity_inbound: int,
            fees_earned_day: int = None,
            fees_earned_week: int = None,
            fees_earned_month: int = None
    ):
        self.channels = channels
        self.capacity = capacity
        self.capacity_outbound = capacity_outbound
        self.capacity_inbound = capacity_inbound
        self.fees_earned_day = fees_earned_day
        self.fees_earned_week = fees_earned_week
        self.fees_earned_month = fees_earned_month


def parse_fees(unparsed_fees) -> (int, int, int):
    return int(unparsed_fees['day_fee_sum']), int(unparsed_fees['week_fee_sum']), int(unparsed_fees['month_fee_sum'])


def get_api_fees_earned() -> (int, int, int):
    api_url = LND_API + '/v1/fees'
    response = requests.get(api_url, headers=HEADERS, verify=CERT_PATH)
    if response.status_code != 200:
        return None, None, None

    json_response = response.json()
    try:
        return parse_fees(json_response)
    except Exception as e:
        print(repr(e), 'Unable to parse fees, skipping: {0}'.format(json_response))
        return None, None, None


def get_channel_stats(channels: [Channel]) -> Stats:
    stat_channel_count = len(channels)
    stat_capacity = 0
    stat_outbound_capacity = 0
    stat_inbound_capacity = 0
    for channel in channels:
        stat_capacity += channel.capacity
        stat_outbound_capacity += channel.local_balance
        stat_inbound_capacity += channel.remote_balance
    return Stats(
        stat_channel_count,
        stat_capacity,
        stat_outbound_capacity,
        stat_inbound_capacity
    )


def get_stats(channels: [Channel]) -> Stats:
    stats = get_channel_stats(channels)

    sleep(1)
    fees_earned_day, fees_earned_week, fees_earned_month = get_api_fees_earned()
    today = datetime.now()
    stats.fees_earned_day = fees_earned_day
    if today.weekday() == 0:  # monday
        stats.fees_earned_week = fees_earned_week
    if today.day == 1:  # first day of the month
        stats.fees_earned_month = fees_earned_month

    return stats
