# channel-to-telegram
## Installation
Clone the git repo:
```
git clone https://bitbucket.org/volkersk/channel-to-telegram.git
```

Navigate to the folder:
```
cd channel-to-telegram
```

Install requirements:
```
pip3 install -r requirements.txt
```

Configure *constants.py*:
```
nano constants.py
```

Configure crontab:
```
crontab -e
55 6 * * * python3 /home/umbrel/channel-to-telegram/channel-to-telegram.py > /dev/null 2>&1
```
